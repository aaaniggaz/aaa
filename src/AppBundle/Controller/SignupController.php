<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use RandomLib\Generator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class SignupController extends Controller
{
    /**
     * @Route("/signup", name="signup")
     * @Method("POST")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function signupAction(Request $request)
    {
        $email    = $request->request->get('email');
        $password = $request->request->get('password');

        // TODO: Validate values

        $user = $this->get('user_repo')->findOneBy([
           'email' => $email

        ]);

        if ($user) {
            return new JsonResponse([], JsonResponse::HTTP_BAD_REQUEST);
        }

        $user = new User();
        $user->setEmail($email);
        $user->setLoggedIn(false);

        $encodedPassword = $this->get('security.password_encoder')->encodePassword($user, $password);
        $user->setPassword($encodedPassword);

        /** @var Generator $tokenGenerator */
        $tokenGenerator = $this->get('random_generator_factory')->getMediumStrengthGenerator();
        // Store abcd... in config
        $token = $tokenGenerator->generateString(64, 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');

        $encodedToken = md5($token);
        $user->setToken($encodedToken);

        // TODO: Validate User

        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();

        return new JsonResponse([
            'token' => $token
        ], JsonResponse::HTTP_OK);
    }
}
