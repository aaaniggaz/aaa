<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class ApiController extends Controller
{
    /**
     * @Route("/login-status", name="getLoginStatus")
     * @Method("GET")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getLoginStatusAction(Request $request)
    {
        $token = $request->query->get('token');

        // TODO: Validate data

        $user = $this->get('user_repo')->findOneBy([
            'token' => md5($token) // Replace with a Hasher implementation
        ]);

        // TODO: Validate user

        if ($user) {
            return new JsonResponse([
                'logged_in' => $user->getLoggedIn()
            ], JsonResponse::HTTP_OK);
        }

        return new JsonResponse([], JsonResponse::HTTP_BAD_REQUEST);
    }

    /**
     * @Route("/login-status", name="toggleLoginStatus")
     * @Method("PUT")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function toggleLoginStatusAction(Request $request)
    {
        $token = $request->request->get('token');

        // TODO: Validate data

        $user = $this->get('user_repo')->findOneBy([
            'token' => md5($token) // Again, better implementation
        ]);

        // TODO: Validate user

        if ($user) {
            $user->toggleLoggedIn();

            $this->getDoctrine()->getManager()->persist($user);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse([
                'logged_in' => $user->getLoggedIn()
            ], JsonResponse::HTTP_OK);
        }

        return new JsonResponse([
            'token' => $token
        ], JsonResponse::HTTP_BAD_REQUEST);
    }
}
