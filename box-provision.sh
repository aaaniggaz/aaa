#!/usr/bin/env bash

# Provision script for vagrant box

cd /vagrant

echo "Installing dependencies..."

composer install

echo "Creating database..."
mysql -uroot -proot -e "CREATE DATABASE aaa_api"

echo "Executing migrations..."
php bin/console doctrine:migrations:migrate

echo "Provisioning complete!"
